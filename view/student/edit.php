<?php
include_once '../include/header.php';
	
	 if (isset($_GET['id'])) {
    }

    include_once '../../vendor/autoload.php';

    $single_view = new App\admin\Product\Product();

    $single_view->set($_GET);

    $single_view_all_id =  $single_view->singleView();

?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit Product</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Basic Product Update Form
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8 col-md-offset-2">
                                <form role="form" action="view/student/update.php" method="POST">
                                    <div class="form-group">
                                        <label>Product Title</label>
                       <input value="<?php echo $single_view_all_id['product_title'] ?>" name="name" class="form-control">
                       <input type="hidden" value="<?php echo $single_view_all_id['id'] ?>" name="name" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Product Price</label>
                                        <input value="<?php echo $single_view_all_id['product_price'] ?>"  name="price" class="form-control">
                                    </div>
                                    <div class="form-group">

                                        <label>Category</label>
                                        <select name="category" class="form-control">
                                            <option >Select One</option>
   <option <?php echo ($single_view_all_id['product_category']=='male')?'selected':'' ?> value="male">Male</option>
   <option <?php echo ($single_view_all_id['product_category']=='female')?'selected':'' ?> value="female">Female</option>
   <option <?php echo ($single_view_all_id['product_category']=='baby')?'selected':'' ?> value="baby">Baby</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                            <textarea name="description" class="form-control" rows="3"><?php echo $single_view_all_id['product_description'] ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Upload Image</label>
                                        <input name="thumbnail" type="file"/>
                                    </div>
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                    <button type="submit" name="submit" class="btn btn-primary">Update</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>