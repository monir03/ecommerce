<?php

    include_once '../include/header.php';

    if (isset($_GET['id'])) {
    }

    include_once '../../vendor/autoload.php';

    $single_view = new App\admin\Product\Product();

    $single_view->set($_GET);

    $single_view_all_id =  $single_view->singleView();


?>

<style>
    .prd span img{
        height: 250px !important;
    }


</style>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row  <div class="col-lg-4 col-lg-offset-4"> -->
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Product Details
                    </div>
                        <div class="panel-body prd">
                            <span class="thumbnail">
                                <img display="block" height="300px" src="view/uploads/<?php echo $single_view_all_id['thumbnail'] ?>" alt="...">
                                <h4><?php echo $single_view_all_id['product_title'] ?></h4>
                                <div class="ratings">
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star"></span>
                                    <span class="glyphicon glyphicon-star-empty"></span>
                                </div>
                                <p><?php echo $single_view_all_id['product_description'] ?></p>
                                <hr class="line">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <p class="price">$<?php echo $single_view_all_id['product_price'] ?></p>
                                    </div>

                                </div>
                            </span>
                    <!-- /.panel-body -->
                    <div class="panel-footer ">

                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <a href="view/student/delete.php?id=<?php echo $single_view_all_id['id'] ?>" class="btn btn-danger">Delete</a>
                                <a href="view/student/edit.php?id=<?php echo $single_view_all_id['id'] ?>" class="btn btn-primary">Edit</a>
                                <a href="" class="btn btn-default">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>