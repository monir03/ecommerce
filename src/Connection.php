<?php
namespace App;
use PDO;
use PDOException;
class Connection
{
    private $user = "root";
    private $password = "";
    protected $db_connection;

    public function __construct()
    {
        try{
            $this->db_connection = new PDO("mysql:host=localhost;dbname=ecommerce","$this->user","$this->password");

            $this->db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }catch (PDOEXception $error){

            echo "Datbase connection error</br>".$error->getMessage();

        }
    }
}