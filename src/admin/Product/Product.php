<?php
namespace App\admin\Product;
use PDO;
use PDOException;
use App\Connection;

class Product extends Connection{

	private $name;
    private $price;
	private $category;
    private $description;
	private $image;
    private $id;

	public function set( array $cath ){

        	if (array_key_exists( 'name', $cath)) {
        		
        		$this->name = $_POST['name'];

        	} 
        	if (array_key_exists( 'category', $cath)) {
        		
        		$this->category = $_POST['category'];

        	} 
        	if (array_key_exists( 'description', $cath)) {
        		
        		$this->description = $_POST['description'];

        	} 
            if (array_key_exists( 'price', $cath)) {
                    
                    $this->price = $_POST['price'];
            }
             if (array_key_exists( 'img', $cath)) {
                    
                    $this->image = $_POST['img'];
            }

            
            if (array_key_exists( 'id', $cath)) {
                    
                    $this->id = $_GET['id'];
            }
	}

	public function storeDb(){

        	try{

                $stmt = $this->db_connection->prepare("INSERT INTO products(product_title, product_price, product_category, product_description, thumbnail) VALUES(:name,:price, :category, :description, :thumbnail )");
              

                $stmt->execute([

        			':name'  	=>$this->name,
                    ':price'        =>$this->price,
        			':category'  	=>$this->category,
                    ':description'  =>$this->description,
                    ':thumbnail'    =>$this->image
        			
        			]);

                
        		
        	}catch(PDOException $error){

        		echo "DB error".$error->getMessage();

        	}
        }


        public function view(){

        	try{

                $stmt = $this->db_connection->prepare("SELECT * FROM products");
                 
                $stmt->execute();

                return $stmt->fetchAll(PDO::FETCH_ASSOC);
        		
        	}catch(PDOException $error){

        	   echo "DB error".$error->getMessage();

        	}
        }


        public function singleView(){

            try{

                $stmt = $this->db_connection->prepare("SELECT * FROM products WHERE id=:id");

                $stmt -> bindValue( ':id', $this->id );
    
                $stmt -> execute();

                return $stmt -> fetch(PDO::FETCH_ASSOC);

            }catch(PDOException $error){

                echo "Database erro".$error->getMessage();

            }


        }

        public function update(){

            try{

                $stmt = $this->db_connection->prepare("UPDATE products SET product_title = :title, product_price =:price, product_category =:category, product_description =:description WHERE = :id");

                $stmt->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
          
                $stmt -> bindValue( ':title', $this->name );
                $stmt -> bindValue( ':price', $this->price );
                $stmt -> bindValue( ':category', $this->category );
                $stmt -> bindValue( ':description', $this->description );
                $stmt -> bindValue( ':id', $this->id );

                return $stmt -> execute();                   

            }catch(PDOException $error){

                echo "Database erro".$error->getMessage();

            }

        }

        public function delete(){

             try{

                $stmt = $this->db_connection->prepare("DELETE FROM products WHERE id=:id");

                $stmt -> bindValue( ':id', $this->id );
    
                $stmt -> execute();
                  

            }catch(PDOException $error){

                    echo "Database erro".$error->getMessage();

            }

        }


        // public function delete_img($id){


        //     try{

        //         $stmt = $this->db_connection->prepare("SELECT `thumbnail` FROM products WHERE id=:id");

        //         $stmt -> bindValue( ':id', $id );
    
        //         $stmt -> execute();

        //         $img_delete = $stmt -> fetch(PDO::FETCH_ASSOC);

        //         if ( isset($img_delete['img']) ) {

        //             unlink( '../../view/uploads/'.$img_delete['img'] );
        //         }

        //     }catch(PDOException $error){

        //         echo "Database erro".$error->getMessage();

        //     }
        // }



}





